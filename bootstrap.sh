#!/bin/bash

BASEPATH=$(cd `dirname $0`; pwd)
cd $BASEPATH

#------------------------------------------------------------
# Basic Installation
#------------------------------------------------------------
ln -s $HOME/dotfiles/.peco              $HOME/
ln -s $HOME/dotfiles/.zsh               $HOME/
ln -s $HOME/dotfiles/.zshrc             $HOME/
ln -s $HOME/dotfiles/.zshrc.alias       $HOME/
ln -s $HOME/dotfiles/.ctags             $HOME/
ln -s $HOME/dotfiles/.gitignore         $HOME/
ln -s $HOME/dotfiles/.vimrc             $HOME/
ln -s $HOME/dotfiles/.tmux.conf         $HOME/
ln -s $HOME/dotfiles/.dir_colors        $HOME/
ln -s $HOME/dotfiles/.vim               $HOME/
ln -s $HOME/dotfiles/.gvimrc            $HOME/
ln -s $HOME/dotfiles/.taskrc            $HOME/
ln -s $HOME/dotfiles/.zshrc.alias.local $HOME/
ln -s $HOME/dotfiles/.tmux.conf.local   $HOME/
git submodule update --init
git submodule foreach git checkout master
vim -N -u $HOME/.vimrc -c "try | NeoBundleUpdate $* | finally | qall! | endtry" -U NONE -i NONE -V1 -e -s


#------------------------------------------------------------
# MacOSX
#------------------------------------------------------------
case "${OSTYPE}" in
    darwin*)
        # homebrew
        ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
        # add repositories
        brew tap caskroom/fonts
        brew tap homebrew/dupes
        brew tap josegonzalez/php
        # nodebrew
        curl -L git.io/nodebrew | perl - setup
        echo export PATH=\$HOME/.nodebrew/current/bin:\$PATH >> ~/.profile
        source ~/.profile
        nodebrew install latest
        nodebrew install stable
        nodebrew use latest
        # tools via brew
        brew install --disable-etcdir zsh            || true
        brew install ack                             || true
        brew install ag                              || true
        brew install android-sdk                     || true
        brew install ansible                         || true
        brew install autojump                        || true
        brew install binutils                        || true
        brew install caskroom/cask/brew-cask         || true
        brew install cocot                           || true
        brew install coreutils                       || true
        brew install ctags                           || true
        brew install curl                            || true
        brew install findutils                       || true
        brew install git                             || true
        brew install go                              || true
        brew install htop                            || true
        brew install hub                             || true
        brew install jq                              || true
        brew install lynx                            || true
        brew install mobile-shell                    || true
        brew install mcrypt                          || true
        brew install nkf                             || true
        brew install openssl                         || true
        brew install peco                            || true
        brew install php55                           || true
        brew install rbenv                           || true
        brew install reattach-to-user-namespace      || true
        brew install task                            || true
        brew install tig                             || true
        brew install tmux                            || true
        brew install tree                            || true
        brew install vim                             || true
        brew install vit                             || true
        brew install watch                           || true
        brew install wget                            || true
        brew install xz                              || true
        brew install zsh                             || true
        echo export "PATH=\$(brew --prefix php55)/bin:\$PATH" >> ~/.profile

        # tools via npm
        npm install -g bower
        #npm install -g coffee-script
        npm install -g gulp
        npm install -g gulp-straw
        npm install -g istanbul
        npm install -g david
        npm install -g node-inspector
        npm install -g superstatic

        # desktop apps via cask
        export HOMEBREW_CASK_OPTS="--appdir=/Applications"
        brew cask install 0xed                       || true
        brew cask install 1password                  || true
        brew cask install alfred                     || true
        brew cask install atom                       || true
        brew cask install balsamiq-mockups           || true
        brew cask install bettertouchtool            || true
        brew cask install brackets                   || true
        brew cask install cheatsheet                 || true
        brew cask install chocolat                   || true
        brew cask install colors                     || true
        brew cask install coteditor                  || true
        brew cask install cyberduck                  || true
        brew cask install dash                       || true
        brew cask install dropbox                    || true
        brew cask install eclipse-ide                || true
        brew cask install evernote                   || true
        brew cask install filezilla                  || true
        brew cask install firefox                    || true
        brew cask install gimp                       || true
        brew cask install github                     || true
        brew cask install google-chrome              || true
        brew cask install google-drive               || true
        brew cask install google-japanese-ime        || true
        brew cask install hipchat                    || true
        brew cask install iterm2                     || true
        brew cask install launchpad-manager-yosemite || true
        brew cask install libreoffice                || true
        brew cask install licecap                    || true
        brew cask install macvim                     || true
        brew cask install marked                     || true
        brew cask install mi                         || true
        brew cask install mou                        || true
        brew cask install mysqlworkbench             || true
        brew cask install night-owl                  || true
        brew cask install pandoc                     || true
        brew cask install querious                   || true
        brew cask install skype                      || true
        brew cask install slack                      || true
        brew cask install sourcetree                 || true
        brew cask install speedlimit                 || true
        brew cask install spectacle                  || true
        brew cask install the-unarchiver             || true
        brew cask install vagrant                    || true
        brew cask install vagrant-manager            || true
        brew cask install virtualbox                 || true
        brew cask install vlc                        || true

        # fonts via caskroom-fonts
        brew cask install font-fontawesome
        brew cask install font-lobster
        brew cask install font-noto-sans-japanese

        # cleanup
        brew update                                  || true
        brew cleanup

        # vagrant
        vagrant plugin install vagrant-global-status
        vagrant plugin install vagrant-hostmanager
        vagrant plugin install vagrant-cachier
        vagrant plugin install sahara
        vagrant plugin install vagrant-vbox-snapshot
        vagrant plugin install vagrant-omnibus

        # zsh
        cat /etc/shells
        echo 'If you use zsh'
        echo '$chsh -s /path/to/zsh'
        ;;
    linux*)
        echo "nothing"
        ;;
esac
