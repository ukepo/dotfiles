## installation

If MacOSX, to install xcode and agree the license.

To install basic dotfiles.

```
$ cd $HOME
$ git clone https://gitlab.com/ukepo/dotfiles.git
$ dotfiles/bootstrap.sh
```

## updation

To update dotfiles with new files.

```
$ cd $HOME/dotfiles
$ git add .
$ git commit -m "comment"
$ git push
```

To update dotfiles without new files.

```
$ cd $HOME/dotfiles
$ git commit -a -m "comment"
$ git push
```

## Synchronization

To sync from others.

```
$ cd $HOME/dotfiles
$ git pull
```
