" colorscheme Tomorrow-Night-Blue
" colorscheme Tomorrow-Night
" set guifont=Ricty:h20
" colorscheme badwolf
colorscheme edark
set guifont=Anonymous\ Pro:h20
" set transparency=4
" set lines=999 columns=999
set lines=400 columns=200
set guioptions-=m
set guioptions-=T
set guioptions-=r
set guioptions-=R
set guioptions-=l
set guioptions-=L
